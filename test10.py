import tensorflow as tf
import numpy as np

# 전처리로,
# window size = M, slinding window로 데이터를 자른다.
# 이 window를 seq_size와 pred_size로 자른 후,
# pred에서 기준(80)이 넘어가는 데이터가 하나라도 있으면 1(on) 아니면 0(off)으로 라벨을 생성,
# seq, 라벨을 입력해서 학습
# 이때 pred에서 파워값이 1인데 기준(80)이 넘어가는 데이터가 하나라도 있으면,
# 파워가 처음 1로 변한 곳으로 이동해서 그 앞쪽 A개의 데이터의 라벨을 1로 설정한다.
# 아니면 라벨을 1로 설정

tf.set_random_seed(777)  # reproducibility

seq_length = 270
predict_length = 45
window_size = seq_length + predict_length
batch_total_size = 4331  # 0 - 227, 1 - 4331, 2 - 4331, 3 - 4321, 4 - 3947, 5 - 4332, 6 - 3282
batch_size = 270 + 45

data_dim = 6
output_dim = 1

num_units = 256
layer = 1
lstm1_num_units_arr = [2048, 1024, 512]
# lstm2_num_units_arr = [64, 64]
learning_rate = 1e-4
keep_prob = 0.7
epoch = 15000

models_root_path = "./models/"
save_path = models_root_path + "test9"
base_path = "nor_logs/timeNMA2/"
paths = [
    base_path + "logs/log_1530539050616.csv",
    base_path + "logs/log_1530543600525.csv",
    base_path + "logs/log_1530630006099.csv",
    base_path + "logs/log_1530716419899.csv",
    base_path + "logs/log_1530802923506.csv",
    base_path + "logs/log_1530889213504.csv",
    base_path + "logs/log_1530975619691.csv"
]

start_file_index = 2
end_file_index = 3
x = np.loadtxt(paths[start_file_index], delimiter=",")
dataset = tf.data.Dataset.from_tensor_slices(x)
for i in range(start_file_index + 1, end_file_index):
    x = np.loadtxt(paths[i], delimiter=",")
    dataset_tmp = tf.data.Dataset.from_tensor_slices(x)
    dataset = dataset.concatenate(dataset_tmp)

dataset = dataset.apply(tf.contrib.data.sliding_window_batch(window_size=window_size, stride=1)).batch(batch_size)
dataset = dataset.repeat()
iterator = dataset.make_one_shot_iterator()
next_element = iterator.get_next()
lstm1_x = next_element[:, :seq_length]
pred = next_element[:, seq_length:, 2:4]
y = next_element[:, seq_length:, 2:4]


with tf.variable_scope("placeholder"):
    lstm1_X = tf.placeholder(tf.float32, [None, seq_length, data_dim], name="lstm1_X")
    Y = tf.placeholder(tf.float32, [None, output_dim], name="Y")
    Keep_prob = tf.placeholder(tf.float32, name="Keep_prob")
    epoch_tf = tf.Variable(0, name="epoch_tf", dtype=tf.int32)

with tf.variable_scope("lstm1"):
    lstms1 = []
    for hidden_dim in lstm1_num_units_arr:
        lstm1 = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True, activation=tf.nn.tanh)
        lstms1.append(tf.contrib.rnn.DropoutWrapper(lstm1, output_keep_prob=Keep_prob))
    # lstms = [tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True) for hidden_dim in lstm1_num_units_arr]
    cells = tf.contrib.rnn.MultiRNNCell(lstms1, state_is_tuple=True)
    # states = get_state_variables(tf.shape(lstm1_x)[0], cells)
    lstm1_outputs, lstm1_state = tf.nn.dynamic_rnn(cell=cells, inputs=lstm1_X, dtype=tf.float32)
    # update_op = get_state_update_op(states, lstm1_state)

with tf.variable_scope("prediction"):
    # dense1_output = tf.layers.dense(lstm1_outputs, num_units, activation=tf.nn.relu)
    # dense2_output = tf.layers.dense(dense1_output, num_units, activation=tf.nn.relu)
    predict = tf.layers.dense(lstm1_outputs[:, -1], output_dim, activation=None, name="predict")
    # predict = tf.identity(lstm1_outputs[:, seq_length:], name="predict")


with tf.variable_scope("train"):
    # loss = tf.reduce_mean(tf.square(Y - predict))
    loss = tf.reduce_mean(tf.squared_difference(Y, predict), name="loss")
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train = optimizer.minimize(loss, name="train")

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()

    try:
        saver.restore(sess, tf.train.latest_checkpoint(models_root_path))
        graph = tf.get_default_graph()
    except ValueError:
        print("First epoch.")

    start_epoch = sess.run(epoch_tf)
    for step in range(start_epoch, epoch):
        _loss = 0
        batch_loop_length = int(batch_total_size / batch_size)
        for batch_step in range(batch_loop_length):
            train_x, train_y = sess.run([lstm1_x, y])
            # npzeros = np.zeros([train_x.shape[0], predict_length, data_dim])
            _, l = sess.run([train, loss],  feed_dict={lstm1_X: train_x, Y: train_y, Keep_prob: keep_prob})
            _loss += l / batch_loop_length
            print(step, "/", batch_loop_length, "/", batch_step, "/", l)

        sess.run(epoch_tf.assign(step + 1))
        if step % 10 == 0:
            saver.save(sess, save_path, global_step=step)
        print("epoch: ", step, "loss: ", _loss)
    # testx, testy = sess.run([lstm1_x, y])
    # print(testx, testy)
    # test_1_x, test_1_y = sess.run([lstm1_x, y])

    # test_1_x, test_1_y = data_set("logs/log_1530543600525.csv")
    # test_1_x = test_1_x[:3]
    # a = sess.run(lstm1_outputs, feed_dict={lstm1_X: test_1_x})
    # print(a)
    # test_2_x, test_2_y = data_set("logs/log_1530543600525.csv")
    # test_2_x = test_2_x[1:4]
    # b = sess.run(lstm1_outputs, feed_dict={lstm1_X: test_2_x})
    # print(b)

    # test_3_x, test_3_y = sess.run([lstm1_x, y])
    # test_3_x = test_3_x[:3]
    # c = sess.run(predict, feed_dict={lstm1_X: test_3_x})
    # print(c)
    # # test_4_x, test_4_y = sess.run([lstm1_x, y])
    # test_4_x = test_3_x[1000:1003]
    # d = sess.run(predict, feed_dict={lstm1_X: test_4_x})
    # print(d)