import numpy as np
import matplotlib.pyplot as plt
from DataManipulator import DataNormalize

filelist = [
    "logs/log_1530539050616.csv",
    "logs/log_1530543600525.csv",
    "logs/log_1530630006099.csv",
    "logs/log_1530716419899.csv",
    "logs/log_1530802923506.csv",
    "logs/log_1530889213504.csv",
    "logs/log_1530975619691.csv"
]
# 기본
'''
for path in filelist:
    dn = DataNormalize()
    data = dn.get_data(path)
    new_data = data[:, -1] + 32400000
    new_data = new_data[:] / 1000
    new_data = new_data[:] % 86400
    new_data1 = np.round(new_data[:] / 20)
    new_data1 = np.array(new_data1, dtype=np.int32)
    new_data1 = new_data1.reshape([np.shape(new_data1)[0], 1])
    data1 = np.hstack([data[:, :-1], new_data1])
    new_data = dn.moving_average(data1, 20, 3)
    # scaled = dn.min_max_scaler(new_data)
    scaled = dn.min_max_scaler(new_data, _min=[-50., 0., 0., 0., 0., 0.], _max=[70., 100., 999., 999., 1., 4320.])
    scaled2 = scaled * 100
    dn.saveCSV("nor_logs/onlytime/" + path, scaled2)
    plt.plot(scaled2[:, 0], "b")
    plt.show()
    plt.plot(data[:, 0], "r")
    plt.show()
    print("?")
'''

# 시간만
for path in filelist:
    dn = DataNormalize()
    data = dn.get_data(path)
    new_data = data[:, -1] + 32400000
    new_data = new_data[:] / 1000
    new_data = new_data[:] % 86400
    new_data1 = np.round(new_data[:] / 20)
    new_data1 = np.array(new_data1, dtype=np.int32)
    new_data1 = new_data1.reshape([np.shape(new_data1)[0], 1])
    new_data1 = dn.min_max_scaler(new_data1, _min=[0.], _max=[4320.])
    new_data1 = new_data1 * 100
    data1 = np.hstack([data[:, :-1], new_data1])
    new_data = dn.moving_average(data1, 15, 3)
    # scaled = dn.min_max_scaler(new_data)
    # scaled = dn.min_max_scaler(new_data, _min=[-50., 0., 0., 0., 0., 0.], _max=[70., 100., 999., 999., 1., 4320.])
    # scaled2 = scaled * 100
    dn.saveCSV("nor_logs/timeNMA2/" + path, new_data)
    # plt.plot(scaled[:, -1], "b")
    # plt.show()
    # plt.plot(data[:, -1], "k")
    # plt.show()
    plt.plot(new_data[:, 0], "b")
    plt.show()
    plt.plot(data[:, 0], "r")
    plt.show()
    print(path + " done")
