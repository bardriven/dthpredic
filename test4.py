import numpy as np
import tensorflow as tf
from DataManipulator import DataNormalize
import matplotlib.pyplot as plt

from SlidingWindow import SlidingWindow

tf.set_random_seed(777)  # reproducibility


# def get_min_max(data):
#     return np.min(data, 0), np.max(data, 0)
#
#
# def rollback(data, _min, _max):
#     return data * (_max - _min + 1e-7) + _min
#
#
# def MinMaxScaler(data):
#     ''' Min Max Normalization
#     Parameters
#     ----------
#     data : numpy.ndarray
#         input data to be normalized
#         shape: [Batch size, dimension]
#     Returns
#     ----------
#     data : numpy.ndarry
#         normalized data
#         shape: [Batch size, dimension]
#     References
#     ----------
#     .. [1] http://sebastianraschka.com/Articles/2014_about_feature_scaling.html
#     '''
#     numerator = data - np.min(data, 0)
#     denominator = np.max(data, 0) - np.min(data, 0)
#     # noise term prevents the zero division
#     return numerator / (denominator + 1e-7)
#
#
# def get_min_max(data):
#     return np.min(data, 0), np.max(data, 0)
#
#
# def rollback(data, _min, _max):
#     return data * (_max - _min + 1e-7) + _min


def reads_file(path):
    file = tf.train.string_input_producer(path, name="filename_queue", shuffle=False)
    text_reader = tf.TextLineReader()
    _, line = text_reader.read(file)
    t, h, p2, p1, p, ti = tf.decode_csv(line, record_defaults=[[0.], [0.], [0.], [0.], [0.], [0.]], field_delim=",")
    return t, h, p2, p1, p, ti


timesteps = seq_length = 75
data_dim = 5
output_dim = 2
predict_length = 0
# batch_size = seq_length + predict_length + 1
onetime_length = 4320
batch_size = 1
hidden_dim = 52
batch_total_size = int(21262 / batch_size)
# batch_total_size = int(4331 / batch_size)
num_layers = 1
learning_rate = 0.01
keep_prob = 0.5
epoch = 4
filelist = ["nor_logs/log_1530543600525.csv",
            "nor_logs/log_1530630006099.csv",
            "nor_logs/log_1530716419899.csv",
            "nor_logs/log_1530802923506.csv",
            "nor_logs/log_1530889213504.csv"
            ]

# def batch_data_set(data):
#     x = data[:, :5]
#     # y = data[:, 2:5]
#
#     # data_x = x[0:seq_length]
#     # data_y = y[seq_length + predict_length - 1]
#     for i in range(batch_size - seq_length - predict_length):
#         _x = x[i:i + seq_length]
#         # _y = y[i + seq_length + predict_length - 1]
#         # print(_x, " -> ", _y)
#         data_x = tf.concat([data_x, _x], 0)
#         # data_y = tf.concat([data_y, _y], 0)
#     data_x = tf.reshape(data_x[seq_length:], [batch_size - seq_length - predict_length, seq_length, data_dim])
#     # data_y = tf.reshape(data_y[output_dim:], [batch_size - seq_length - predict_length, output_dim])
#     # data_x.append(_x)
#     # data_y.append(_y)
#     return data_x# , data_y


def read_file(path):
    return np.loadtxt(path, delimiter=",")


def data_set(xy):
    x = xy[:, :5]
    y = xy[:, 2:4]

    dataX = []
    dataY = []
    for i in range(len(y) - seq_length - predict_length):
        _x = x[i:i + seq_length]
        _y = y[i + seq_length + predict_length]
        # print(_x, " -> ", _y)
        dataX.append(_x)
        dataY.append(_y)

    return np.array(dataX), np.array(dataY)


temperature, humidity, p2_5, p10, power, time = reads_file(filelist)
b_data = tf.train.batch([[temperature, humidity, p2_5, p10, power]], batch_size=batch_size, shapes=[data_dim])
# trainX, trainY = batch_data_set(b_data)

# trainX, trainY = data_set("logs/log_1530630006099.csv")

X = tf.placeholder(tf.float32, [None, seq_length, data_dim])
Y = tf.placeholder(tf.float32, [None, output_dim])
Keep_prob = tf.placeholder(tf.float32)

cells = [tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True) for lstm in range(num_layers)]
# cells = [tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell(num_units=hidden_dim) for lstm in range(num_layers)]
drops = [tf.contrib.rnn.DropoutWrapper(lstm, output_keep_prob=keep_prob) for lstm in cells]
cell = tf.contrib.rnn.MultiRNNCell(drops, state_is_tuple=True)
#cell = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True)
outputs, _state = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
Y_pred = tf.contrib.layers.fully_connected(outputs[:, -1], output_dim, activation_fn=None)
a = tf.square(Y_pred - Y)
loss = tf.reduce_sum(a)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train = optimizer.minimize(loss)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    sliding_window = SlidingWindow(size=seq_length + predict_length + 1)
    for i in range(epoch):
        _loss = 0
        for step in range(int(batch_total_size / onetime_length)):
            tX = []
            tY = []
            for one in range(onetime_length):
                while sliding_window.is_done:
                    sliding_window.add_array(sess.run(b_data))
                tD = sliding_window.next()
                taaa = tD[:seq_length]
                tX.append(tD[:seq_length])
                tY.append(tD[-1, 2:4])
            tX, tY = np.array(tX), np.array(tY)
            _, l = sess.run([train, loss], feed_dict={X: tX, Y: tY, Keep_prob: keep_prob})
            _loss += l / (batch_total_size / onetime_length)
            print(i, "/", int(batch_total_size / onetime_length), "/", step, "/", l)
        print("epoch: ", i, ", loss: ", _loss)

    coord.request_stop()
    coord.join(threads)

    testData = read_file("nor_logs/log_1530975619691.csv")
    # __min, __max = get_min_max(testData)
    testX, testY = data_set(testData)
    # testY = rollback(testY, __min[2:4], __max[2:4])
    testPredict = sess.run(Y_pred, feed_dict={X: testX, Keep_prob: 1})
    DN = DataNormalize()
    testPredict = DN.moving_average(testPredict, 12)
    # testPredict = rollback(testPredict, __min[2:4], __max[2:4])
    plt.plot(testY[:, 0], "b")
    plt.plot(testPredict[:, 0], "g")
    plt.show()
    plt.plot(testY[:, 1], "r")
    plt.plot(testPredict[:, 1], "y")
    plt.show()
    # plt.plot(testY[:, 2], "k")
    # plt.plot(testPredict[:, 2], "g")
    # plt.show()
