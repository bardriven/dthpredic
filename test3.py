import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import urllib.request
import json

tf.set_random_seed(777)  # reproducibility


def min_max_scaler(data):
    n = data - np.min(data, 0)
    d = np.max(data, 0) - np.min(data, 0)
    return n / (d + 1e-7)


batch_size = 4331# 24200
timesteps = seq_length = 7
data_dim = 5
output_seq_length = 1
output_dim = 3

hidden_dim = 20


def data_set(data, mms=True):
    if mms:
        data = min_max_scaler(data)
    x = data[:, :5]
    y = data[:, 2:5]

    data_x = []
    data_y = []
    for i in range(len(y) - seq_length - output_seq_length):
        _x = x[i:i + seq_length]
        _y = y[i + seq_length:i + seq_length + output_seq_length]
        # print(_x, " -> ", _y)
        data_x.append(_x)
        data_y.append(_y)
    r1, r2 = np.array(data_x), np.array(data_y)
    r2 = np.reshape(r2, [np.shape(r2)[0], output_seq_length * output_dim])
    return r1, r2


def real_time_data():
    rj = urllib.request.urlopen("http://dev.osirptpm.ml:3000/getNormalizedDTH").read().decode("utf8")
    rj = json.loads(rj)
    rd = []
    for h in rj:
        _p = 0.
        if h["state"]["reported"]["power"]:
            _p = 1.
        rd.append([
            h["state"]["reported"]["temperature"],
            h["state"]["reported"]["humidity"],
            h["state"]["reported"]["pm2_5"],
            h["state"]["reported"]["pm10"],
            _p
        ])
    return np.array(rd)


def read_file(path):
    return np.loadtxt(path, delimiter=",")


def read_files(path):
    file = tf.train.string_input_producer(path, name="filename_queue", shuffle=False)
    text_reader = tf.TextLineReader()
    _, line = text_reader.read(file)
    t, h, p2, p1, p, ti = tf.decode_csv(line, record_defaults=[[0.], [0.], [0.], [0.], [0.], [0.]], field_delim=",")
    return t, h, p2, p1, p, ti


file_list = [
    # "logs/log_1530539050616.csv",
    "logs/log_1530543600525.csv",
    # "logs/log_1530630006099.csv",
    # "logs/log_1530716419899.csv",
    # "logs/log_1530802923506.csv",
    # "logs/log_1530889213504.csv",
    # "logs/log_1530975619691.csv"
]
temperature, humidity, p2_5, p10, power, time = read_files(file_list)
b_data = tf.train.batch([[temperature, humidity, p2_5, p10, power]], batch_size=batch_size, shapes=[5])

# data = read_file("logs/log_1530630006099.csv")
# trainX, trainY = data_set(data)

X = tf.placeholder(tf.float32, [None, seq_length, data_dim])
Y = tf.placeholder(tf.float32, [None, output_seq_length * output_dim])
num_layers = 1
cell = []
for _ in range(num_layers):
    cell.append(tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True))
cell = tf.contrib.rnn.MultiRNNCell(cell, state_is_tuple=True)

outputs, _state = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)

output_reshape = tf.reshape(outputs[:, output_seq_length:seq_length, 2:5], [tf.shape(outputs)[0], output_seq_length * output_dim])

Y_pred = tf.contrib.layers.fully_connected(output_reshape, output_seq_length * output_dim, activation_fn=None)

a = tf.square(Y_pred - Y)
loss = tf.reduce_sum(a)
optimizer = tf.train.AdamOptimizer(0.005)
train = optimizer.minimize(loss)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    test_data = sess.run(b_data)
    train_x, train_y = data_set(test_data)
    #print(train_x, train_y)
    for step in range(1000):
        _, lo = sess.run([train, loss], feed_dict={X: train_x, Y: train_y})
        print(step, lo)

    coord.request_stop()
    coord.join(threads)

    testX, testY = data_set(read_file("logs/log_1530539050616.csv"))
    testPredict = sess.run(Y_pred, feed_dict={X: testX})
    # testPredict = np.reshape(testPredict, [np.shape(testPredict)[0], 15, 3])
    # print(testPredict)
    pm2_5_testY = testY[:, 0:-1:3]
    pm2_5_testPredict = testPredict[:, 0:-1:3]
    plt.plot(pm2_5_testY[:, 0], "b")
    plt.plot(pm2_5_testPredict[:, 0], "g")
    plt.show()
    pm10_testY = testY[:, 1:-1:3]
    pm10_testPredict = testPredict[:, 1:-1:3]
    plt.plot(pm10_testY[:, 0], "r")
    plt.plot(pm10_testPredict[:, 0], "y")
    plt.show()
    power_testY = testY[:, 2:-1:3]
    power_testPredict = testPredict[:, 2:-1:3]
    plt.plot(power_testY[:, 0], "k")
    plt.plot(power_testPredict[:, 0], "w")
    plt.show()
