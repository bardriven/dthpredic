from collections import deque
import copy
import itertools
import numpy as np


class DataNormalize:
    def __init__(self, original_path=None, new_path=None):
        self.original_path = original_path
        self.new_path = new_path
        self._min = None
        self._max = None

    def run_mms(self):
        if self.original_path is not None and self.new_path is not None:
            data = self.get_data(self.original_path)
            new_data = self.min_max_scaler(data)
            self.saveCSV(self.new_path, new_data)
        else:
            raise ValueError("path error")

    def run_ma(self):
        if self.original_path is not None and self.new_path is not None:
            data = self.get_data(self.original_path)
            new_data = self.moving_average(data)
            self.saveCSV(self.new_path, new_data)
        else:
            raise ValueError("path error")

    def get_data(self, path):
        return np.loadtxt(path, delimiter=",")

    def min_max_scaler(self, data, _min=None, _max=None):
        if _min is None:
            self._min = np.min(data, 0)
        else:
            self._min = np.array(_min)
        if _max is None:
            self._max = np.max(data, 0)
        else:
            self._max = np.array(_max)
        n = data - self._min
        d = self._max - self._min
        return n / (d + 1e-7)

    def min_max_roll_back(self, data):
        return data * (self._max - self._min + 1e-7) + self._min

    def moving_average(self, ori_data, step=1, scope=3):
        data = np.copy(ori_data)
        for s in range(step):
            pre_value = deque(np.copy(data[:scope]))
            for i in range(len(data) - scope):
                if i < scope:
                    pre = np.sum(list(itertools.islice(pre_value, 0, i)), 0)
                    nex = np.sum(data[i+1:i+1+scope], 0)
                    cur = (pre + nex) / (i + scope)
                    data[i] = copy.deepcopy(cur)
                    continue
                cur = np.sum(pre_value + data[i+1:i+1+scope], 0) / (scope * 2)
                pre_value.append(copy.deepcopy(data[i]))
                pre_value.popleft()
                data[i] = copy.deepcopy(cur)
            for j in range(scope):
                pre = np.sum(pre_value, 0)
                nex = np.sum(data[len(data) - scope + j + 1:len(data)], 0)
                cur = (pre + nex) / (scope * 2 - j - 1)
                data[len(data) - scope + j] = copy.deepcopy(cur)

        return data

    def saveCSV(self, path, data):
        np.savetxt(path, data, delimiter=",")


# path = "logs/log_1530975619691.csv"
# dn = DataNormalize()
# data = dn.getData(path)
# new_data = dn.moving_average(data, 12)
# scaled = dn.min_max_scaler(new_data)
# unscaled = dn.min_max_roll_back(scaled)
# print(unscaled)
# dn.saveCSV("nor_" + path, scaled)


class SlidingWindow:
    def __init__(self, size=7, stride=1):
        self.arr = None
        self.size = size
        self.stride = stride

    def next(self):
        if self.is_done:
            raise EOFError("Done.")
        result = self.arr[:self.size]
        self.arr = self.arr[self.stride:]
        return result

    def add_array(self, add_array):
        if self.arr is None:
            self.arr = add_array
        else:
            self.arr = np.concatenate([self.arr, add_array])

    @property
    def is_done(self):
        if self.arr is None:
            return True
        return len(self.arr) < self.size


class DataBatch(SlidingWindow):
    def __init__(self, batch_size=1, stride=1, batch_length=1):
        super().__init__(batch_size, stride)
        self.batch_size = batch_size
        self.stride = stride
        self.batch_length = batch_length

    def next_batch(self):
        return np.array([super(DataBatch, self).next() for _ in range(self.batch_length)])

    @property
    def is_batch_done(self):
        if self.arr is None:
            return True
        return len(self.arr) < self.batch_size + self.batch_length
