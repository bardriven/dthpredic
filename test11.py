# <저장>
# builder = tf.saved_model.builder.SavedModelBuilder(model_path)
# builder.add_meta_graph_and_variables(sess,[tf.saved_model.tag_constants.SERVING])
# builder.save()
#
# <호출>
# tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], model_path)
#
# # Saving
# builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
# builder.add_meta_graph_and_variables(sess, ["tag"], signature_def_map= {
#     "model": tf.saved_model.signature_def_utils.predict_signature_def(
#         inputs= {"x": x},
#         outputs= {"finalnode": model})
# })
# builder.save()
#
# # loading
# with tf.Session(graph=tf.Graph()) as sess:
#     tf.saved_model.loader.load(sess, ["tag"], export_dir)
#     graph = tf.get_default_graph()
#     x = graph.get_tensor_by_name("x:0")
#     model = graph.get_tensor_by_name("finalnode:0")
#     print(sess.run(model, {x: [5, 6, 7, 8]}))

import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


def get_file_list(dirname):
    return os.listdir(dirname)


def get_batch_xy(path):
    start_file_index = 3
    end_file_index = 4
    total_line = 0

    paths = get_file_list(path)
    paths = [path + i for i in paths]
    x = np.loadtxt(paths[start_file_index], delimiter=",")
    total_line += x.shape[0]
    dataset = tf.data.Dataset.from_tensor_slices(x)
    for i in range(start_file_index + 1, end_file_index):
        x = np.loadtxt(paths[i], delimiter=",")
        print("file number", i)
        total_line += x.shape[0]
        dataset_tmp = tf.data.Dataset.from_tensor_slices(x)
        dataset = dataset.concatenate(dataset_tmp)

    dataset = dataset.apply(tf.contrib.data.sliding_window_batch(window_size=window_size, stride=1)).batch(batch_size)
    dataset = dataset.repeat()
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()
    lstm1_nonpad_x = next_element[:, :seq_length]
    paddings = tf.constant([[0, 0], [0, predict_length], [0, 0]])
    lstm1_x = tf.pad(lstm1_nonpad_x, paddings, "CONSTANT")
    # lstm2_x = next_element[:, seq_length:, 2:4]
    y = next_element[:, seq_length:, 2:4]
    return lstm1_x, y, total_line


with tf.variable_scope("const"):
    seq_length = 50
    predict_length = 1
    window_size = seq_length + predict_length

    data_dim = 6
    output_dim = 2
    # num_units = 256
    lstm1_num_units_arr = [256, 256]

    learning_rate = 1e-4
    keep_prob = 1
    epoch = 2001

    # batch_total_size = 0 # 0 - 227, 1 - 4331, 2 - 4331, 3 - 4321, 4 - 3947, 5 - 4332, 6 - 3282
    batch_size = 1000

    models_root_path = "./models/"
    save_path = models_root_path + "test11"
    base_path = "nor_logs/timeNMA2/"
    suffix = "logs/"
    path = base_path + suffix


with tf.variable_scope("batch_process"):
    lstm1_x, y, batch_total_size = get_batch_xy(path)


with tf.variable_scope("placeholder"):
    lstm1_X = tf.placeholder(tf.float32, [None, window_size, data_dim], name="lstm1_X")
    Y = tf.placeholder(tf.float32, [None, predict_length, output_dim], name="Y")
    Keep_prob = tf.placeholder(tf.float32, name="Keep_prob")
    epoch_tf = tf.Variable(0, name="epoch_tf", dtype=tf.int32)

with tf.variable_scope("lstm1"):
    lstms1 = []
    for hidden_dim in lstm1_num_units_arr:
        lstm1 = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True, activation=tf.nn.tanh)
        lstms1.append(tf.contrib.rnn.DropoutWrapper(lstm1, output_keep_prob=Keep_prob))
    # lstms = [tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True) for hidden_dim in lstm1_num_units_arr]
    cells = tf.contrib.rnn.MultiRNNCell(lstms1, state_is_tuple=True)
    # states = get_state_variables(tf.shape(lstm1_x)[0], cells)
    lstm1_outputs, lstm1_state = tf.nn.dynamic_rnn(cell=cells, inputs=lstm1_X, dtype=tf.float32)
    # update_op = get_state_update_op(states, lstm1_state)

with tf.variable_scope("prediction"):
    # dense1_output = tf.layers.dense(lstm1_outputs, num_units, activation=tf.nn.relu)
    # dense2_output = tf.layers.dense(dense1_output, num_units, activation=tf.nn.relu)
    predict = tf.layers.dense(lstm1_outputs[:, seq_length:], output_dim, activation=None, name="predict")
    # predict = tf.identity(lstm1_outputs[:, seq_length:], name="predict")

with tf.variable_scope("train"):
    # loss = tf.reduce_mean(tf.square(Y - predict))
    loss = tf.reduce_mean(tf.squared_difference(Y, predict), name="loss")
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train = optimizer.minimize(loss, name="train")


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    saver = tf.train.Saver()

    try:
        saver.restore(sess, tf.train.latest_checkpoint(models_root_path))
        graph = tf.get_default_graph()
    except ValueError:
        print("First epoch.")

    start_epoch = sess.run(epoch_tf)
    for step in range(start_epoch, epoch):
        _loss = 0
        batch_loop_length = int(batch_total_size / batch_size)
        if batch_total_size % batch_size > 0:
            batch_loop_length += 1

        for batch_step in range(batch_loop_length):
            train_x, train_y = sess.run([lstm1_x, y])
            # npzeros = np.zeros([train_x.shape[0], predict_length, data_dim])
            _, l = sess.run([train, loss],  feed_dict={lstm1_X: train_x, Y: train_y, Keep_prob: keep_prob})
            _loss += l / batch_loop_length
            print(step, "/", batch_loop_length, "/", batch_step, "/", l)

        sess.run(epoch_tf.assign(step + 1))
        if step % 50 == 0:
            saver.save(sess, save_path, global_step=step)
        print("epoch: ", step, "loss: ", _loss)

    prediction_inputs = tf.saved_model.utils.build_tensor_info(
        lstm1_X)
    prediction_inputs_Keep_prob = tf.saved_model.utils.build_tensor_info(
        Keep_prob)

    prediction_outputs = tf.saved_model.utils.build_tensor_info(
        predict)

    prediction_signature = (
        tf.saved_model.signature_def_utils.build_signature_def(
            inputs={
                # tf.saved_model.signature_constants.PREDICT_INPUTS:
                "lstm1_X":
                    prediction_inputs,
                "Keep_prob": prediction_inputs_Keep_prob
            },
            outputs={
                tf.saved_model.signature_constants.PREDICT_OUTPUTS:
                    prediction_outputs
            },
            method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))

    builder = tf.saved_model.builder.SavedModelBuilder(save_path + "/2")  # version
    builder.add_meta_graph_and_variables(sess, [tf.saved_model.tag_constants.SERVING], signature_def_map={
        # "model": tf.saved_model.signature_def_utils.predict_signature_def(
        #     inputs={"lstm1_X": lstm1_X, "Keep_prob": tf.constant(1.)},
        #     outputs={"predict": predict}),
        tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
            prediction_signature,
    }, strip_default_attrs=True)
    builder.save()

# builder = tf.saved_model.builder.SavedModelBuilder(export_path)
# builder.add_meta_graph_and_variables(
#     sess, [tag_constants.SERVING],
#     signature_def_map={
#         'predict_images':
#             prediction_signature,
#         signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
#             classification_signature,
#     },
#     legacy_init_op=legacy_init_op)
# builder.save()
#