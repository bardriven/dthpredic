import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import matplotlib.pyplot as plt

tf.set_random_seed(777)  # reproducibility

def MinMaxScaler(data):
    ''' Min Max Normalization
    Parameters
    ----------
    data : numpy.ndarray
        input data to be normalized
        shape: [Batch size, dimension]
    Returns
    ----------
    data : numpy.ndarry
        normalized data
        shape: [Batch size, dimension]
    References
    ----------
    .. [1] http://sebastianraschka.com/Articles/2014_about_feature_scaling.html
    '''
    numerator = data - np.min(data, 0)
    denominator = np.max(data, 0) - np.min(data, 0)
    # noise term prevents the zero division
    return numerator / (denominator + 1e-7)


timesteps = seq_length = 7
data_dim = 5
output_dim = 3

hidden_dim = 20


def data_set(path):
    xy = np.loadtxt(path, delimiter=",")
    #xy = xy[::-1]
    xy = MinMaxScaler(xy)
    x = xy[:, :5]
    y = xy[:, 2:5]

    dataX = []
    dataY = []
    for i in range(len(y) - seq_length):
        _x = x[i:i + seq_length]
        _y = y[i + seq_length]
        # print(_x, " -> ", _y)
        dataX.append(_x)
        dataY.append(_y)

    return np.array(dataX), np.array(dataY)
    #train_size = int(len(dataY) * 0.7)
    #test_size = len(dataY) - train_size
    #print(train_size, test_size)
    #trainX, testX = np.array(dataX[0:train_size]), np.array(dataX[train_size:len(dataX)])
    #trainY, testY = np.array(dataY[0:train_size]), np.array(dataY[train_size:len(dataY)])


trainX, trainY = data_set("logs/log_1530630006099.csv")

X = tf.placeholder(tf.float32, [None, seq_length, data_dim])
Y = tf.placeholder(tf.float32, [None, 3])
num_layers = 1
cell = []
for i in range(num_layers):
    cell.append(tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True))
cell = tf.contrib.rnn.MultiRNNCell(cell, state_is_tuple=True)

#cell = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True)
outputs, _state = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
Y_pred = tf.contrib.layers.fully_connected(outputs[:, -1], output_dim, activation_fn=None)
a = tf.square(Y_pred - Y)
loss = tf.reduce_sum(a)
optimizer = tf.train.AdamOptimizer(0.01)
train = optimizer.minimize(loss)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(1000):
        _, l = sess.run([train, loss], feed_dict={X: trainX, Y: trainY})
        print(i, l)

    testX, testY = data_set("logs/log_1530975619691.csv")
    testPredict = sess.run(Y_pred, feed_dict={X: testX})
    # testPredict = sess.run(outputs, feed_dict={X: testX[:3]})
    # testPredict2 = sess.run(outputs, feed_dict={X: testX[1000:1003]})
    plt.plot(testY[:, 0], "b")
    plt.plot(testPredict[:, 0], "g")
    plt.show()
    plt.plot(testY[:, 1], "r")
    plt.plot(testPredict[:, 1], "y")
    plt.show()
    plt.plot(testY[:, 2], "k")
    plt.plot(testPredict[:, 2], "w")
    plt.show()
    print(0)

## todo 결과 잘나옴 이를 바탕으로 개선 필요
