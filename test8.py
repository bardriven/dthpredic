import tensorflow as tf
import numpy as np


tf.set_random_seed(777)  # reproducibility

seq_length = 135
predict_length = 45
batch_total_size = 4331 + 4331 + 4321 + 3947  # 0 - 227, 1 - 4331, 2 - 4331, 3 - 4321, 4 - 3947, 5 - 4332, 6 - 3282
batch_size = int(batch_total_size / 25)
window_size = seq_length + predict_length

data_dim = 6
output_dim = 2

lstm1_num_units_arr = [64, 64]
lstm2_num_units_arr = [64, 64]
# layer = 2
learning_rate = 0.05
keep_prob = 0.7
epoch = 10000

models_root_path = "./models/"
save_path = models_root_path + "test8"
base_path = "nor_"
paths = [
    base_path + "logs/log_1530539050616.csv",
    base_path + "logs/log_1530543600525.csv",
    base_path + "logs/log_1530630006099.csv",
    base_path + "logs/log_1530716419899.csv",
    base_path + "logs/log_1530802923506.csv",
    base_path + "logs/log_1530889213504.csv",
    base_path + "logs/log_1530975619691.csv"
]

start_file_index = 1
end_file_index = 5
x = np.loadtxt(paths[start_file_index], delimiter=",")
dataset = tf.data.Dataset.from_tensor_slices(x)
for i in range(start_file_index + 1, end_file_index):
    x = np.loadtxt(paths[i], delimiter=",")
    dataset_tmp = tf.data.Dataset.from_tensor_slices(x)
    dataset = dataset.concatenate(dataset_tmp)

dataset = dataset.apply(tf.contrib.data.sliding_window_batch(window_size=window_size, stride=1)).batch(batch_size)
dataset = dataset.repeat()
iterator = dataset.make_one_shot_iterator()
next_element = iterator.get_next()
lstm1_x = next_element[:, :seq_length]
# lstm2_x = next_element[:, seq_length:, 2:4]
y = next_element[:, seq_length:, 2:4]


with tf.variable_scope("placeholder"):
    lstm1_X = tf.placeholder(tf.float64, [None, seq_length, data_dim], name="lstm1_X")
    lstm2_X = tf.placeholder(tf.float64, [None, predict_length, output_dim], name="lstm2_X")
    Y = tf.placeholder(tf.float64, [None, predict_length, output_dim], name="Y")
    Keep_prob = tf.placeholder(tf.float64, name="Keep_prob")
    epoch_tf = tf.Variable(0, name="epoch_tf", dtype=tf.int32)

with tf.variable_scope("lstm1"):
    lstms1 = []
    for hidden_dim in lstm1_num_units_arr:
        lstm1 = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True)
        lstms1.append(tf.contrib.rnn.DropoutWrapper(lstm1, output_keep_prob=Keep_prob))
    # lstms = [tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True) for hidden_dim in lstm1_num_units_arr]
    cells = tf.contrib.rnn.MultiRNNCell(lstms1, state_is_tuple=True)

    lstm1_outputs, lstm1_state = tf.nn.dynamic_rnn(cell=cells, inputs=lstm1_X, dtype=tf.float64)

with tf.variable_scope("lstm2"):
    lstms2 = []
    for hidden_dim in lstm1_num_units_arr:
        lstm2 = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True)
        lstms2.append(tf.contrib.rnn.DropoutWrapper(lstm2, output_keep_prob=Keep_prob))
    cells = tf.contrib.rnn.MultiRNNCell(lstms2, state_is_tuple=True)

    lstm2_outputs, lstm2_state = tf.nn.dynamic_rnn(cell=cells, inputs=lstm2_X, initial_state=lstm1_state, dtype=tf.float64)

with tf.variable_scope("prediction"):
    # predict = tf.contrib.layers.fully_connected(outputs[:, -1], output_dim)
    predict = tf.layers.dense(lstm2_outputs, output_dim, activation=None, name="predict")


with tf.variable_scope("train"):
    # loss = tf.reduce_mean(tf.square(Y - predict))
    loss = tf.reduce_mean(tf.squared_difference(Y, predict), name="loss")
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train = optimizer.minimize(loss, name="train")

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()

    try:
        saver.restore(sess, tf.train.latest_checkpoint(models_root_path))
        graph = tf.get_default_graph()
    except ValueError:
        print("First epoch.")

    start_epoch = sess.run(epoch_tf)
    for step in range(start_epoch, epoch):
        _loss = 0
        batch_loop_length = int(batch_total_size / batch_size)
        for batch_step in range(batch_loop_length):
            train_x, train_y = sess.run([lstm1_x, y])
            npzeros = np.zeros([train_x.shape[0], predict_length, output_dim])
            _, l, _state = sess.run([train, loss, lstm1_state],  feed_dict={lstm1_X: train_x, lstm2_X: npzeros, Y: train_y, Keep_prob: keep_prob})
            _loss += l / batch_loop_length
            print(_state)
            print(step, "/", batch_loop_length, "/", batch_step, "/", l)
        if step % 10 == 0:
            saver.save(sess, save_path, global_step=step)
        sess.run(epoch_tf.assign(step + 1))
        print("epoch: ", step, "loss: ", _loss)
    # testx, testy = sess.run([lstm1_x, y])
    # print(testx, testy)
    # test_1_x, test_1_y = sess.run([lstm1_x, y])

    # test_1_x, test_1_y = data_set("logs/log_1530543600525.csv")
    # test_1_x = test_1_x[:3]
    # a = sess.run(lstm1_outputs, feed_dict={lstm1_X: test_1_x})
    # print(a)
    # test_2_x, test_2_y = data_set("logs/log_1530543600525.csv")
    # test_2_x = test_2_x[1:4]
    # b = sess.run(lstm1_outputs, feed_dict={lstm1_X: test_2_x})
    # print(b)

    # test_3_x, test_3_y = sess.run([lstm1_x, y])
    # test_3_x = test_3_x[:3]
    # c = sess.run(predict, feed_dict={lstm1_X: test_3_x})
    # print(c)
    # # test_4_x, test_4_y = sess.run([lstm1_x, y])
    # test_4_x = test_3_x[1000:1003]
    # d = sess.run(predict, feed_dict={lstm1_X: test_4_x})
    # print(d)