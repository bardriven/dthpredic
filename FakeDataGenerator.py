#기본식
#y=g^x
#y=ax+b
#y=cx^2+dx+h
#y=f^(-x)

#기본조건
import random
import math
import sys


max = 10
#max = sys.float_info.max
min = 0
'''
g = 0
a = 0
c = 0
d = 0
h = 0
f = 0
b = 0
x1 = 0
x2 = 0
x3 = 0
y1 = 0
y2 = 0
y3 = 0
'''

def generator():
    while 1:
        g = random.random() * (max - min + 1) + min
        a = random.random() * (max - min + 1) + min
        c = -random.random() * (max - min + 1) + min
        d = random.random() * (max - min + 1) + min
        h = -random.random() * (max - min + 1) + min
        f = random.random() * (max - min + 1) + min
        if g > 1 and a > 0 > c and pow(d, 2) - 4 * c * h > 0 and d > 0 > h and f > 0 and f != 1:
            #if a / math.log(g) > 0:
            x1 = math.log(a / math.log(g), g)
            b = pow(g, x1) - a * x1
            if h < b:
                x2 = math.sqrt((h - b) / c)
                if x1 < x2:
                    y2 = c * pow(x2, 2) + d * x2 + h
                    #y2 = a * x2 + b
                    if y2 > 0:
                        x3 = (-2 * c - d * math.log(f) + math.sqrt(4 * pow(c, 2) + pow(d, 2) * pow(math.log(f), 2) + 4 * c * h * pow(math.log(f), 2))) / 2 * c * math.log(f)
                        if x2 < x3:
                            print(f, -x3)
                            try:
                                y1 = a * x1 + b
                                #y1 = pow(g, x1)
                                y3 = c * pow(x3, 2) + d * x3 + h
                                #y3 = pow(f, -x3)
                                print("y = " + str(g) + "^x")
                                print("y = " + str(a) + " * x + " + str(b))
                                print("y = " + str(c) + " * x^2 + " + str(d) + "x + " + str(h))
                                print("y = " + str(f) + "^(-x)")
                                print("(x1, y1) = (" + str(x1) + ", " + str(y1) + ")")
                                print("(x2, y2) = (" + str(x2) + ", " + str(y2) + ")")
                                print("(x3, y3) = (" + str(x3) + ", " + str(y3) + ")")
                                break
                            except OverflowError:
                                print(OverflowError)
                        #else:
                            #generator()
                    #else:
                        #generator()
                #else:
                    #generator()
            #else:
                #generator()
        #else:
            #generator()


generator()
'''

g>0
a>0
c<0
(d^2)-4*c*h>0
d>0  #-d/c>0
h<0  #h/c>0
f>0




x>0
y>0

#점 조건
y1=g^x1
y1=a*x1+b

y2=a*x2+b
y2=c*x2^2+d*x2+h

y3=c*x3^2+d*x3+h
y3=f^(-x3)



#연속조건
g^(x1)==a*(x1)+b
c*(x2)^2+(d-a)*(x2)+e-b==0
c*(x3)^2+d*(x3)+h==f^(-(x3))

#미분가능조건
(g^(x1))*log(g)==a
a==2*c*(x2)+d
2*c*(x3)+d==(f^(-(x3)))*log(f)

'''


'''
class FakeDataGenerator:
    def __init__(self):
        self.step = 0
        self.temperature = 0
        self.humidity = 0
        self.pm2_5 = 0
        self.pm10 = 0

    def get_data(self):
        return {
            "temperature": self.temperature,
            "humidity": self.humidity,
            "pm2_5": self.pm2_5,
            "pm10": self.pm10,
        }

    def _generate_value(self):
        random.random()
        self.temperature = 0
        self.humidity = 0
        self.pm2_5 = 0
        self.pm10 = 0
'''