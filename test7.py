import numpy as np
import tensorflow as tf
from DataManipulator import DataNormalize, DataBatch
import matplotlib.pyplot as plt


tf.set_random_seed(777)  # reproducibility


def reads_file(path):
    file = tf.train.string_input_producer(path, name="filename_queue", shuffle=False)
    text_reader = tf.TextLineReader()
    _, line = text_reader.read(file)
    t, h, p2, p1, p, ti = tf.decode_csv(line, record_defaults=[[0.], [0.], [0.], [0.], [0.], [0.]], field_delim=",")
    return t, h, p2, p1, p, ti


seq_length = 360
data_dim = 6
output_dim = 2
hidden_dim = 52
predict_length = 90

batch_size = seq_length + predict_length
batch_length = 537
# batch_total_size = int(21262 / batch_length)
batch_total_size = int(4331 / batch_length)
epoch = 10

models_root_path = "./models/"
save_path = models_root_path + "test7"

learning_rate = 0.008
keep_prob = 0.8
layer = 2

filelist = ["nor_logs/log_1530543600525.csv",
            # "nor_logs/log_1530630006099.csv",
            # "nor_logs/log_1530716419899.csv",
            # "nor_logs/log_1530802923506.csv",
            # "nor_logs/log_1530889213504.csv"
            ]

temperature, humidity, p2_5, p10, power, _time = reads_file(filelist)
b_data = tf.train.batch([[temperature, humidity, p2_5, p10, power, _time]], batch_size=batch_length, shapes=[data_dim])

e_input = tf.placeholder(tf.float32, [None, seq_length, data_dim], name="e_input")
d_input = tf.placeholder(tf.float32, [None, predict_length, output_dim], name="d_input")
Y = tf.placeholder(tf.float32, [None, predict_length * output_dim], name="Y")
Keep_prob = tf.placeholder(tf.float32, name="Keep_prob")

epoch_tf = tf.Variable(0, name="epoch_tf", dtype=tf.int32)

with tf.variable_scope("encode"):
    e_drops = []
    for _ in range(layer):
        e_cell = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True)
        e_drops.append(tf.contrib.rnn.DropoutWrapper(e_cell, output_keep_prob=Keep_prob))

    e_cells = tf.contrib.rnn.MultiRNNCell(e_drops, state_is_tuple=True)
    outputs, e_states = tf.nn.dynamic_rnn(e_cells, e_input, dtype=tf.float32)

with tf.variable_scope("decode"):
    d_drops = []
    for _ in range(layer):
        d_cell = tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True)
        # d_drops.append(tf.nn.rnn_cell.DropoutWrapper(d_cell, output_keep_prob=Keep_prob))
        d_drops.append(tf.contrib.rnn.DropoutWrapper(d_cell, output_keep_prob=Keep_prob))

    d_cells = tf.contrib.rnn.MultiRNNCell(d_drops, state_is_tuple=True)
    outputs, d_states = tf.nn.dynamic_rnn(d_cells, d_input, initial_state=e_states, dtype=tf.float32)

with tf.variable_scope("prediction"):
    # predict = tf.layers.dense(outputs, output_dim, activation=None, name="predict")
    outputs = tf.reshape(outputs, [tf.shape(outputs)[0], predict_length * hidden_dim])
    predict = tf.contrib.layers.fully_connected(outputs, predict_length * output_dim, activation_fn=None, scope="predict")

with tf.variable_scope("train"):
    # loss = tf.losses.mean_squared_error(predictions=predict, labels=Y)
    # loss = tf.reduce_mean(tf.squared_difference(Y, predict), name="loss")
    loss = tf.reduce_sum(tf.square(predict - Y), name="loss")
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train = optimizer.minimize(loss, name="train")

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()

    try:
        saver.restore(sess, tf.train.latest_checkpoint(models_root_path))
        graph = tf.get_default_graph()
    except ValueError:
        print("First epoch.")

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    data_normalize = DataNormalize()
    data_batch = DataBatch(batch_size=batch_size, stride=1, batch_length=batch_length)

    start_epoch = sess.run(epoch_tf)
    for i in range(start_epoch, epoch):
        _loss = 0
        for step in range(int(batch_total_size)):
            while data_batch.is_batch_done:
                data_batch.add_array(sess.run(b_data))
            tD = data_batch.next_batch()
            tX = tD[:, :seq_length]
            tY = tD[:, seq_length:seq_length + predict_length, 2:4]
            tX, tY = np.array(tX), np.array(tY)
            tY = np.reshape(tY, [batch_length, predict_length * output_dim])
            _, l = sess.run([train, loss], feed_dict={e_input: tX, d_input: np.zeros([batch_length, predict_length, output_dim]), Y: tY, Keep_prob: keep_prob})
            _loss += l / batch_total_size
            print(i, "/", batch_total_size, "/", step, "/", l)
        sess.run(epoch_tf.assign(i + 1))
        saver.save(sess, save_path, global_step=i)
        print("epoch: ", i, ", loss: ", _loss)

    coord.request_stop()
    coord.join(threads)

    # data_normalize_test = DataNormalize()
    # testData = data_normalize_test.get_data("nor_logs/log_1530543600525.csv")
    # data_batch_test = DataBatch(batch_size=batch_size, stride=1, batch_length=batch_length)
    # data_batch_test.add_array(testData)
    # batch_test = data_batch_test.next_batch()
    # testX = batch_test[:, :seq_length]
    # testY = batch_test[:, seq_length:seq_length + predict_length, 2:4]
    # test_predict = sess.run(predict, feed_dict={e_input: testX, d_input: np.zeros([batch_length, predict_length, output_dim]), Keep_prob: 1})
    # testPredict = data_normalize_test.moving_average(test_predict[0], 10)
    # # testPredict = rollback(testPredict, __min[2:4], __max[2:4])
    #
    # plt.ion()
    # fig = plt.figure()
    # ax1 = fig.add_subplot(211)
    # ax2 = fig.add_subplot(212)
    #
    # for j in range(len(testX)):
    #     ax1.clear()
    #     ax2.clear()
    #     testPredict = data_normalize_test.moving_average(test_predict[j], 10)
    #     line1, = ax1.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 2], testY[j, :, 0])), 'b')
    #     line2, = ax1.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 2], testY[j, :, 0])), 'g')
    #     line3, = ax2.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 3], testY[j, :, 1])), 'r')
    #     line4, = ax2.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 3], testY[j, :, 1])), 'y')
    #
    #     line1.set_ydata(np.hstack((testX[j, :, 2], testY[j, :, 0])))
    #     line2.set_ydata(np.hstack((testX[j, :, 2], testPredict[:, 0])))
    #     line3.set_ydata(np.hstack((testX[j, :, 3], testY[j, :, 1])))
    #     line4.set_ydata(np.hstack((testX[j, :, 3], testPredict[:, 1])))
    #     fig.canvas.draw()
