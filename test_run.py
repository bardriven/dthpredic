from DataManipulator import DataNormalize, DataBatch
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from time import sleep

# seq_length = 30
# data_dim = 6
# output_dim = 2
# hidden_dim = 52
# predict_length = 30
#
# batch_size = seq_length + predict_length
# batch_length = 1
# batch_total_size = int(4331 / batch_length)

seq_length = 7
predict_length = 1
window_size = seq_length + predict_length
batch_total_size = 227  # 0 - 227, 1 - 4331, 2 - 4331, 3 - 4321, 4 - 3947, 5 - 4332, 6 - 3282
batch_size = int(batch_total_size / 1)
window_size = seq_length + predict_length

data_dim = 6
output_dim = 1

keep_prob = 1

sess = tf.Session()

new_saver = tf.train.import_meta_graph("models/test11-0 - 복사본.meta")
new_saver.restore(sess, tf.train.latest_checkpoint("./models/"))
# tf.reset_default_graph()

graph = tf.get_default_graph()

lstm1_X = graph.get_tensor_by_name("placeholder/lstm1_X:0")
# lstm2_X = graph.get_tensor_by_name("placeholder/lstm2_X:0")
Y = graph.get_tensor_by_name("placeholder/Y:0")
Keep_prob = graph.get_tensor_by_name("placeholder/Keep_prob:0")
#
# for op in tf.get_default_graph().get_operations():
#     print(op.name)
#
predict = graph.get_tensor_by_name("prediction/predict/BiasAdd:0")  # add /BiasAdd:0
# predict = graph.get_tensor_by_name("prediction/predict:0")

base_path = "nor_logs/timeNMA2/"
path = [base_path + "logs/log_1530539050616.csv",
        base_path + "logs/log_1530543600525.csv",
        base_path + "logs/log_1530630006099.csv",
        base_path + "logs/log_1530716419899.csv",
        base_path + "logs/log_1530802923506.csv",
        base_path + "logs/log_1530889213504.csv",
        base_path + "logs/log_1530975619691.csv"
        ]
x = np.loadtxt(path[0], delimiter=",")

dataset = tf.data.Dataset.from_tensor_slices(x)
dataset = dataset.apply(tf.contrib.data.sliding_window_batch(window_size=window_size, stride=1)).batch(batch_size)
dataset = dataset.repeat()
iterator = dataset.make_one_shot_iterator()
next_element = iterator.get_next()
lstm1_nonpad_x = next_element[:, :seq_length]
paddings = tf.constant([[0, 0], [0, predict_length], [0, 0]])
lstm1_x = tf.pad(lstm1_nonpad_x, paddings, "CONSTANT")
# lstm2_x = next_element[:, seq_length:, 2:4]
y = next_element[:, seq_length:, 2:3]

plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(111)
# ax2 = fig.add_subplot(212)

data_normalize_test = DataNormalize()
test_X, test_Y = sess.run([lstm1_x, y])
# testPredict = sess.run(predict, feed_dict={lstm1_X: test_X, Keep_prob: keep_prob})
for step in range(len(test_X)):
    testX, testY = test_X[step:step + 1], test_Y[step:step + 1] \
    # test_predict = testPredict[step:step+1]
    # testX = batch_test[:, :seq_length]
    # testY = batch_test[:, seq_length:seq_length + predict_length, 2:4]
    # npzeros = np.zeros([testX.shape[0], predict_length, output_dim])

    test_predict = sess.run(predict, feed_dict={lstm1_X: testX, Keep_prob: keep_prob})

    # testPredict = rollback(testPredict, __min[2:4], __max[2:4])
    # test_predict = np.reshape(test_predict, [1, predict_length, output_dim])
    # test_predict = data_normalize_test.moving_average(test_predict.reshape([predict_length, output_dim]), 12).reshape([1, predict_length, output_dim])

    ax1.clear()
    # ax2.clear()
    testX = testX[:, :seq_length]
    pm2_5 = np.hstack((testX[0, :, 2], testY[0, :, 0]))
    pm2_5_pred = np.hstack((testX[0, :, 2], test_predict[0, :, 0]))
    # pm2_5_pred = data_normalize_test.moving_average(pm2_5_pred[0], 10).reshape([pm2_5_pred.shape[0], pm2_5_pred.shape[1], pm2_5_pred.shape[2]])

    # pm10 = np.hstack((testX[0, :, 3], testY[0, :, 1]))
    # pm10_pred = np.hstack((testX[0, :, 3], test_predict[0, :, 1]))

    # pm10_pred = data_normalize_test.moving_average(pm10_pred[0], 10).reshape([pm10_pred.shape[0], pm10_pred.shape[1], pm10_pred.shape[2]])

    line1, = ax1.plot(np.arange(step, window_size+step, 1), pm2_5, 'b')
    line2, = ax1.plot(np.arange(step, window_size+step, 1), pm2_5_pred, 'g')
    # line3, = ax2.plot(np.arange(step, window_size+step, 1), pm10, 'r')
    # line4, = ax2.plot(np.arange(step, window_size+step, 1), pm10_pred, 'y')

    line1.set_ydata(pm2_5)
    line2.set_ydata(pm2_5_pred)
    # line3.set_ydata(pm10)
    # line4.set_ydata(pm10_pred)
    fig.canvas.draw()
    # sleep(0.1)

