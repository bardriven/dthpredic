import numpy as np


class SlidingWindow:
    def __init__(self, size=7, stride=1):
        self.arr = None
        self.size = size
        self.stride = stride

    def next(self):
        result = self.arr[:self.size]
        self.arr = self.arr[self.stride:]
        return result

    def add_array(self, add_array):
        if self.arr is None:
            self.arr = add_array
        self.arr = np.concatenate([self.arr, add_array])

    @property
    def is_done(self):
        if self.arr is None:
            return True
        return len(self.arr) < self.size