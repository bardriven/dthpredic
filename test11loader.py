import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

models_root_path = "./models/"
save_path = models_root_path + "test11"


def get_file_list(dirname):
    return os.listdir(dirname)


def get_batch_xy(path):
    start_file_index = 6
    end_file_index = 7
    total_line = 0

    paths = get_file_list(path)
    paths = [path + i for i in paths]
    x = np.loadtxt(paths[start_file_index], delimiter=",")
    total_line += x.shape[0]
    dataset = tf.data.Dataset.from_tensor_slices(x)
    for i in range(start_file_index + 1, end_file_index):
        x = np.loadtxt(paths[i], delimiter=",")
        total_line += x.shape[0]
        dataset_tmp = tf.data.Dataset.from_tensor_slices(x)
        dataset = dataset.concatenate(dataset_tmp)

    dataset = dataset.apply(tf.contrib.data.sliding_window_batch(window_size=window_size, stride=1)).batch(batch_size)
    dataset = dataset.repeat()
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()
    lstm1_nonpad_x = next_element[:, :seq_length]
    paddings = tf.constant([[0, 0], [0, predict_length], [0, 0]])
    lstm1_x = tf.pad(lstm1_nonpad_x, paddings, "CONSTANT")
    # lstm2_x = next_element[:, seq_length:, 2:4]
    y = next_element[:, seq_length:, 2:4]
    return lstm1_x, y, total_line


with tf.variable_scope("const"):
    seq_length = 50
    predict_length = 1
    window_size = seq_length + predict_length

    # data_dim = 6
    # output_dim = 1
    # # num_units = 256
    # lstm1_num_units_arr = [128]
    #
    # learning_rate = 1e-4
    # keep_prob = 1
    # epoch = 3000

    # batch_total_size = 0 # 0 - 227, 1 - 4331, 2 - 4331, 3 - 4321, 4 - 3947, 5 - 4332, 6 - 3282
    batch_size = 1000

    models_root_path = "./models/"
    save_path = models_root_path + "test11"
    base_path = "nor_logs/timeNMA2/"
    suffix = "logs/"
    path = base_path + suffix


with tf.Session(graph=tf.Graph()) as sess:
    tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], save_path + "/1")
    graph = tf.get_default_graph()
    x = graph.get_tensor_by_name("placeholder/lstm1_X:0")
    Keep_prob = graph.get_tensor_by_name("placeholder/Keep_prob:0")
    model = graph.get_tensor_by_name("prediction/predict/BiasAdd:0")
    lstm1_x, y, batch_total_size = get_batch_xy(path)
    test_x, test_y = sess.run([lstm1_x, y])
    test_predict = sess.run(model, {x: test_x, Keep_prob: 1})


plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

# testPredict = sess.run(predict, feed_dict={lstm1_X: test_X, Keep_prob: keep_prob})
for step in range(len(test_x)):
    testX, testY = test_x[step:step + 1], test_y[step:step + 1]
        # test_predict = testPredict[step:step+1]
    # testX = batch_test[:, :seq_length]
    # testY = batch_test[:, seq_length:seq_length + predict_length, 2:4]
    # npzeros = np.zeros([testX.shape[0], predict_length, output_dim])

    # testPredict = rollback(testPredict, __min[2:4], __max[2:4])
    # test_predict = np.reshape(test_predict, [1, predict_length, output_dim])
    # test_predict = data_normalize_test.moving_average(test_predict.reshape([predict_length, output_dim]), 12).reshape([1, predict_length, output_dim])

    ax1.clear()
    ax2.clear()
    testX = testX[:, :seq_length]
    pm2_5 = np.hstack((testX[0, :, 2], testY[0, :, 0]))
    pm2_5_pred = np.hstack((testX[0, :, 2], test_predict[step, :, 0]))
    # pm2_5_pred = data_normalize_test.moving_average(pm2_5_pred[0], 10).reshape([pm2_5_pred.shape[0], pm2_5_pred.shape[1], pm2_5_pred.shape[2]])

    pm10 = np.hstack((testX[0, :, 3], testY[0, :, 1]))
    pm10_pred = np.hstack((testX[0, :, 3], test_predict[step, :, 1]))

    # pm10_pred = data_normalize_test.moving_average(pm10_pred[0], 10).reshape([pm10_pred.shape[0], pm10_pred.shape[1], pm10_pred.shape[2]])

    line1, = ax1.plot(np.arange(step, window_size+step, 1), pm2_5, 'b')
    line2, = ax1.plot(np.arange(step, window_size+step, 1), pm2_5_pred, 'g')
    line3, = ax2.plot(np.arange(step, window_size+step, 1), pm10, 'r')
    line4, = ax2.plot(np.arange(step, window_size+step, 1), pm10_pred, 'y')

    line1.set_ydata(pm2_5)
    line2.set_ydata(pm2_5_pred)
    line3.set_ydata(pm10)
    line4.set_ydata(pm10_pred)
    fig.canvas.draw()
    # sleep(0.1)

