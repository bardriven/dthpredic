import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt



def MinMaxScaler(data):
    numerator = data - np.min(data, 0)
    denominator = np.max(data, 0) - np.min(data, 0)
    # noise term prevents the zero division
    return numerator / (denominator + 1e-7)


def data_set(path):
    xy = np.loadtxt(path, delimiter=",")
    #xy = xy[::-1]
    xy = MinMaxScaler(xy)
    x = xy[:, :5]
    y = xy[:, 2:5]

    dataX = []
    dataY = []
    for i in range(len(y) - seq_length):
        _x = x[i:i + seq_length]
        _y = y[i + seq_length]
        print(_x, " -> ", _y)
        dataX.append(_x)
        dataY.append(_y)

    return np.array(dataX), np.array(dataY)


def read_file(path):
    file = tf.train.string_input_producer(path, name="filename_queue", shuffle=False)
    text_reader = tf.TextLineReader()
    _, line = text_reader.read(file)
    t, h, p2, p1, p, ti = tf.decode_csv(line, record_defaults=[[0.], [0.], [0.], [0.], [0.], [0.]], field_delim=",")
    return t, h, p2, p1, p, ti


training_epochs = 10
batch_size = 14
seq_length = 7
total_batch_size = int(3282 / batch_size)

data_dim = 5
output_dim = 3
hidden_dim = 20

temperature, humidity, p2_5, p10, power, time = read_file(["logs/log_1530975619691.csv"])
b_data = tf.train.batch([[temperature, humidity, p2_5, p10, power]], batch_size=batch_size, shapes=[5])
b_x_data = b_data
b_y_data = b_data[:, 2:5]
# tf.summary.scalar('b_temperature', tf.reduce_mean(b_temperature))
# tf.summary.scalar('b_humidity', tf.reduce_mean(b_humidity))
# tf.summary.scalar('b_p2_5', tf.reduce_mean(b_p2_5))
# tf.summary.scalar('b_p10', tf.reduce_mean(b_p10))
# tf.summary.scalar('b_power', tf.reduce_mean(b_power))
# tf.summary.scalar('b_time', tf.reduce_mean(b_time))
#
# b_x_data = tf.stack([[b_temperature], [b_humidity], [b_p2_5], [b_p10], [b_power]], 2)
# b_y_data = tf.stack([[b_p2_5[-1], b_p10[-1], b_power[-1]]])


X = tf.placeholder(tf.float32, [None, seq_length, data_dim])
Y = tf.placeholder(tf.float32, [None, 3])

num_layers = 1
cell = []
for i in range(num_layers):
    cell.append(tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True))
cell = tf.contrib.rnn.MultiRNNCell(cell, state_is_tuple=True)

outputs, _state = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
Y_pred = tf.contrib.layers.fully_connected(outputs[:, -1], output_dim, activation_fn=None)

loss = tf.reduce_sum(tf.square(Y_pred - Y))
optimizer = tf.train.AdamOptimizer(0.01)
train = optimizer.minimize(loss)

tf.summary.scalar('loss', loss)


class SlidingWindow:

    def __init__(self, size=7, stride=1):
        self.arr = None
        self.size = size
        self.stride = stride

    def next(self):
        result = self.arr[:self.size]
        self.arr = self.arr[self.stride:]
        return result

    def add_array(self, add_array):
        if self.arr is None:
            self.arr = add_array
        self.arr = np.concatenate([self.arr, add_array])

    @property
    def is_done(self):
        return len(self.arr) < self.size


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    merged = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter("D:/IntellijProjects/tensorflow" + "/train", sess.graph)

    sliding_window = SlidingWindow()

    for e in range(training_epochs):
        avg_loss = 0
        for step in range(total_batch_size):
            x_data, y_data = sess.run([b_x_data, b_y_data])
            sliding_window.add_array(x_data)
            while True:
                if sliding_window.is_done:
                    break
                data = sliding_window.next()
                x_data = data.reshape([1, seq_length, data_dim])
                y_data = data[-1, 2:5].reshape([1, output_dim])
                #print(x_data, y_data)
                _, l, m = sess.run([train, loss, merged], feed_dict={X: x_data, Y: y_data})
                train_writer.add_summary(m, global_step=e * total_batch_size + step)

        print(e)
        #print(e, ", loss: ", avg_loss / total_batch_size)

    coord.request_stop()
    coord.join(threads)

    testX, testY = data_set("logs/log_1530539050616.csv")
    testPredict = sess.run(Y_pred, feed_dict={X: testX})
    plt.plot(testY[:, 0], "b")
    plt.plot(testPredict[:, 0], "g")
    plt.show()
    plt.plot(testY[:, 1], "r")
    plt.plot(testPredict[:, 1], "y")
    plt.show()
    plt.plot(testY[:, 2], "k")
    plt.plot(testPredict[:, 2], "w")
    plt.show()

