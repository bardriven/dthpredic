import time
import numpy as np
import tensorflow as tf
from DataManipulator import DataNormalize, DataBatch
import matplotlib.pyplot as plt

tf.set_random_seed(777)  # reproducibility


def reads_file(path):
    file = tf.train.string_input_producer(path, name="filename_queue", shuffle=False)
    text_reader = tf.TextLineReader()
    _, line = text_reader.read(file)
    t, h, p2, p1, p, ti = tf.decode_csv(line, record_defaults=[[0.], [0.], [0.], [0.], [0.], [0.]], field_delim=",")
    return t, h, p2, p1, p, ti


timesteps = seq_length = 90
data_dim = 6
output_dim = 2
hidden_dim = 128
predict_length = 45

batch_size = seq_length + predict_length
batch_length = 288
# batch_total_size = int(21262 / batch_length)
batch_total_size = int(4331 / batch_length)
# batch_total_size = 25
num_layers = 2
learning_rate = 0.01
keep_prob = 0.5
epoch = 20
filelist = ["nor_logs/log_1530543600525.csv",
            # "nor_logs/log_1530630006099.csv",
            # "nor_logs/log_1530716419899.csv",
            # "nor_logs/log_1530802923506.csv",
            # "nor_logs/log_1530889213504.csv"
            ]

temperature, humidity, p2_5, p10, power, _time = reads_file(filelist)
b_data = tf.train.batch([[temperature, humidity, p2_5, p10, power, _time]], batch_size=batch_length, shapes=[data_dim])
# trainX, trainY = batch_data_set(b_data)

# trainX, trainY = data_set("logs/log_1530630006099.csv")

X = tf.placeholder(tf.float32, [None, seq_length, data_dim])
Y = tf.placeholder(tf.float32, [None, predict_length, output_dim])
Keep_prob = tf.placeholder(tf.float32)

cells = [tf.contrib.rnn.BasicLSTMCell(num_units=hidden_dim, state_is_tuple=True) for lstm in range(num_layers)]
# cells = [tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell(num_units=hidden_dim) for lstm in range(num_layers)]
drops = [tf.contrib.rnn.DropoutWrapper(lstm, output_keep_prob=keep_prob) for lstm in cells]
cell = tf.contrib.rnn.MultiRNNCell(drops, state_is_tuple=True)
outputs, _state = tf.nn.dynamic_rnn(cell, X, dtype=tf.float32)
Y_pred = tf.stack([tf.contrib.layers.fully_connected(outputs[:, seq_length - predict_length + _], output_dim, activation_fn=None) for _ in range(predict_length)], axis=1)
loss = 0
for _ in range(predict_length):
    loss += tf.reduce_sum(tf.square(Y_pred[_] - Y))
# Y_pred = tf.concat(values=Y_preds, axis=0)
# loss = tf.losses.mean_squared_error(Y)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train = optimizer.minimize(loss)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    data_normalize = DataNormalize()
    data_batch = DataBatch(batch_size=batch_size, stride=1, batch_length=batch_length)
    for i in range(epoch):
        _loss = 0
        for step in range(int(batch_total_size)):
            while data_batch.is_batch_done:
                data_batch.add_array(sess.run(b_data))
            tD = data_batch.next_batch()
            tX = tD[:, :seq_length]
            tY = tD[:, seq_length:seq_length + predict_length, 2:4]
            tX, tY = np.array(tX), np.array(tY)
            _, l = sess.run([train, loss], feed_dict={X: tX, Y: tY, Keep_prob: keep_prob})
            _loss += l / batch_total_size
            print(i, "/", batch_total_size, "/", step, "/", l)
        saver.save(sess, './models/fc_test5', global_step=epoch)
        print("epoch: ", i, ", loss: ", _loss)

    coord.request_stop()
    coord.join(threads)

    data_normalize_test = DataNormalize()
    testData = data_normalize_test.get_data("nor_logs/log_1530975619691.csv")
    data_batch_test = DataBatch(batch_size=batch_size, stride=1, batch_length=batch_length)
    data_batch_test.add_array(testData)
    batch_test = data_batch_test.next_batch()
    testX = batch_test[:, :seq_length]
    testY = batch_test[:, seq_length:seq_length + predict_length, 2:4]
    test_predict = sess.run(Y_pred, feed_dict={X: testX, Keep_prob: 1})
    testPredict = data_normalize_test.moving_average(test_predict[0], 10)
    # testPredict = rollback(testPredict, __min[2:4], __max[2:4])

    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)

    for j in range(len(testX)):
        ax1.clear()
        ax2.clear()
        testPredict = data_normalize_test.moving_average(test_predict[j], 10)
        line1, = ax1.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 2], testY[j, :, 0])), 'b')
        line2, = ax1.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 2], testY[j, :, 0])), 'g')
        line3, = ax2.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 3], testY[j, :, 1])), 'r')
        line4, = ax2.plot(np.arange(j, batch_size+j, 1), np.hstack((testX[j, :, 3], testY[j, :, 1])), 'y')

        line1.set_ydata(np.hstack((testX[j, :, 2], testY[j, :, 0])))
        line2.set_ydata(np.hstack((testX[j, :, 2], testPredict[:, 0])))
        line3.set_ydata(np.hstack((testX[j, :, 3], testY[j, :, 1])))
        line4.set_ydata(np.hstack((testX[j, :, 3], testPredict[:, 1])))
        # plt.plot(np.hstack((testX[j, :, 2], testY[j, :, 0])), "b")
        # plt.plot(np.hstack((testX[j, :, 2], testPredict[:, 0])), "g")
        # plt.show()
        # plt.plot(np.hstack((testX[j, :, 3], testY[j, :, 1])), "r")
        # plt.plot(np.hstack((testX[j, :, 3], testPredict[:, 1])), "y")
        # plt.show()
        fig.canvas.draw()
    # plt.plot(testY[:, 2], "k")
    # plt.plot(testPredict[:, 2], "g")
    # plt.show()
